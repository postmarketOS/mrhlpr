#!/bin/sh
# SPDX-License-Identifier: GPL-3.0-or-later

case "$1" in
	pre-commit)
		;;
	post-commit)
		if ! grep -qE "^\.mrtest-" /etc/apk/world; then
			exit 0
		fi

		echo "*"
		echo "* You have installed some packages via mrtest!"
		echo "* These packages are pinned to the MR version and will not be updated by apk."
		echo "*"

		grep -E "^\.mrtest-" /etc/apk/world | sed 's/^/* /'

		echo "*"
		echo "* To check which packages are pinned, use 'apk info -R .mrtest-xxx'"
		echo "* Use 'mrtest zap' to clean up and allow updating them again."
		echo "*"
esac
